declare class FileInfoType {
    baseDir?: string;
    extensionFiles?: string;
}
export default class CfgMedium {
    protected nodeEnv: string;
    protected envFile: string;
    protected reqFile: string;
    protected file: FileInfoType;
    protected requiredFileName: string;
    private params;
    constructor();
    init(param?: FileInfoType): void;
    protected requireCfgEnv(configs_env_filename: string): Object | Error;
    protected requireCnfReq(): Object | Error | {};
    protected parameterCheck(reqParam: string[], envParam: string[]): string[] | null;
    get(param: string): any;
    protected configureFileInfo(fileInfo?: FileInfoType): void;
}
export {};
