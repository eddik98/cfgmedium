'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
class FileInfoType {
    constructor() {
        this.baseDir = './';
        this.extensionFiles = '.json';
    }
}
class CfgMedium {
    constructor() {
        this.nodeEnv = process.env.NODE_ENV;
        this.file = new FileInfoType();
        this.requiredFileName = 'required';
        if (_.isUndefined(this.nodeEnv)) {
            throw new Error('In the variable NODE_ENV,' +
                ' you did not specify the required environment.' +
                ' Example: NODE_ENV=production.');
        }
        this.configureFileInfo();
    }
    init(param) {
        if (!_.isUndefined(param)) {
            this.configureFileInfo(param);
        }
        let cfgEnv = this.requireCfgEnv(this.envFile);
        let cfgReq = this.requireCnfReq();
        if (!_.isEmpty(cfgReq)) {
            let notReqParam = this.parameterCheck(_.keys(cfgReq), _.keys(cfgEnv));
            if (notReqParam !== null) {
                throw new Error('In the file: ' + this.envFile +
                    '. Missing such required parameters: ' + _.join(notReqParam));
            }
        }
        this.params = cfgEnv;
    }
    requireCfgEnv(configs_env_filename) {
        try {
            const cfgEnv = require(configs_env_filename);
            if (_.isEmpty(cfgEnv)) {
                throw new Error(configs_env_filename + '. Is empty!');
            }
            return cfgEnv;
        }
        catch (e) {
            if (e.name === 'SyntaxError') {
                throw new Error('Incorrect JSON file syntax. (Error: SyntaxError)');
            }
            else if (e.code === 'MODULE_NOT_FOUND') {
                throw new Error(e.message + '. Create this file.');
            }
            else {
                throw e;
            }
        }
    }
    requireCnfReq() {
        try {
            return require(this.reqFile);
        }
        catch (e) {
            if (e.name === 'SyntaxError') {
                throw new Error('Incorrect JSON file syntax. (Error: SyntaxError)');
            }
            else {
                return {};
            }
        }
    }
    parameterCheck(reqParam, envParam) {
        let tmp = [];
        _.forEach(reqParam, value => {
            if (envParam.indexOf(value) === -1) {
                tmp.push(value);
            }
        });
        return (tmp.length === 0) ? null : tmp;
    }
    get(param) {
        if (_.isEmpty(param)) {
            throw new Error('Argument is empty!');
        }
        if (!_.has(this.params, param)) {
            throw new Error('Parameter missing: ' + param);
        }
        return this.params[param];
    }
    configureFileInfo(fileInfo) {
        if (!_.isUndefined(fileInfo)) {
            if (!_.isUndefined(fileInfo.baseDir)) {
                this.file.baseDir = fileInfo.baseDir + '/';
            }
            if (!_.isUndefined(fileInfo.extensionFiles)) {
                this.file.extensionFiles = fileInfo.extensionFiles;
            }
        }
        this.reqFile = this.file.baseDir +
            this.requiredFileName +
            this.file.extensionFiles;
        this.envFile = this.file.baseDir +
            this.nodeEnv +
            this.file.extensionFiles;
    }
}
exports.default = CfgMedium;
