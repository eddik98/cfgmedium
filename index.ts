'use strict';
import * as _ from "lodash";

class FileInfoType {
  public baseDir?: string = './';
  public extensionFiles?: string = '.json';
}

export default class CfgMedium {
  protected nodeEnv: string = process.env.NODE_ENV;
  protected envFile: string;
  protected reqFile: string;

  protected file: FileInfoType = new FileInfoType();

  protected requiredFileName: string = 'required';

  private params: object;

  constructor() {
    if (_.isUndefined(this.nodeEnv)) {
      throw new Error('In the variable NODE_ENV,' +
        ' you did not specify the required environment.' +
        ' Example: NODE_ENV=production.');
    }

    this.configureFileInfo();
  }

  public init(param?: FileInfoType): void {
    if (!_.isUndefined(param)) {
      this.configureFileInfo(param);
    }

    let cfgEnv = this.requireCfgEnv(this.envFile);
    let cfgReq = this.requireCnfReq();

    if (!_.isEmpty(cfgReq)) {
      let notReqParam = this.parameterCheck(_.keys(cfgReq), _.keys(cfgEnv));

      if (notReqParam !== null) {
        throw new Error('In the file: ' + this.envFile +
          '. Missing such required parameters: ' + _.join(notReqParam));
      }
    }

    this.params = cfgEnv;
  }

  protected requireCfgEnv(configs_env_filename: string): Object | Error {
    try {
      const cfgEnv = require(configs_env_filename);

      if (_.isEmpty(cfgEnv)) {
        throw new Error(configs_env_filename + '. Is empty!');
      }

      return cfgEnv;
    } catch (e) {
      if (e.name === 'SyntaxError') {
        throw new Error('Incorrect JSON file syntax. (Error: SyntaxError)');
      } else if (e.code === 'MODULE_NOT_FOUND') {
        throw new Error(e.message + '. Create this file.');
      } else {
        throw e;
      }
    }
  }

  protected requireCnfReq(): Object | Error | {} {
    try {
      return require(this.reqFile);
    } catch (e) {
      if (e.name === 'SyntaxError') {
        throw new Error('Incorrect JSON file syntax. (Error: SyntaxError)');
      } else {
        return {};
      }
    }
  }

  protected parameterCheck(reqParam: string[], envParam: string[]): string[] | null {
    let tmp = [];

    _.forEach(reqParam, value => {
      if (envParam.indexOf(value) === -1) {
        tmp.push(value);
      }
    });

    return (tmp.length === 0) ? null : tmp;
  }

  public get(param: string): any {
    if (_.isEmpty(param)) {
      throw new Error('Argument is empty!');
    }

    if (!_.has(this.params, param)) {
      throw new Error('Parameter missing: ' + param);
    }

    return this.params[param];
  }

  protected configureFileInfo(fileInfo?: FileInfoType) {
    if (!_.isUndefined(fileInfo)) {
      if (!_.isUndefined(fileInfo.baseDir)) {
        this.file.baseDir = fileInfo.baseDir + '/'
      }

      if (!_.isUndefined(fileInfo.extensionFiles)) {
        this.file.extensionFiles = fileInfo.extensionFiles
      }
    }

    this.reqFile = this.file.baseDir +
      this.requiredFileName +
      this.file.extensionFiles;

    this.envFile = this.file.baseDir +
      this.nodeEnv +
      this.file.extensionFiles;
  }
}